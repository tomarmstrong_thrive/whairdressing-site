<?php
/**
 * Template Name: Homepage Template
 */

$page_id = get_the_ID();

$featured_image = wp_get_attachment_image_src(get_post_thumbnail_id($page_id), 'single-post-thumbnail');

$metabox_id_array = array(
  'whair_homepage_secondary_section_img',
  'whair_homepage_secondary_section_head_content',
  'whair_homepage_secondary_section_content',
  'whair_homepage_secondary_section_button_text',
  'whair_homepage_secondary_section_button_link',
  'whair_homepage_secondary_section_floating_img',

  'whair_homepage_tripple_cta_content',
  'whair_homepage_cta_image_left',
  'whair_homepage_cta_link_left',
  'whair_homepage_cta_image_center',
  'whair_homepage_cta_link_center',
  'whair_homepage_cta_image_right',
  'whair_homepage_cta_link_right',
);

$metabox_content_array = get_metabox_content($page_id, $metabox_id_array);
?>

<?php while (have_posts()) : the_post(); ?>
  <div class="hero-wrapper" style="background-image:url('<?php echo $featured_image[0] ?>');">
    <div class="hero-wrapper-verticle">
      <div style="display: table-cell; vertical-align: middle;">
        <div class="container">
          <div class="row">
            <div class="col-lg-8 mx-auto">
    		      <div class="hero-content"><?php echo the_content() ?></div>
            </div>
          </div>
        </div>
        <div class="down-arrow fade-3p5s">
          <div id="scroll-down">
            <span class="arrow-down">
            <!-- css generated icon -->
            </span>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="secondary-image-split">
    <div class="row">
      <div class="col-xl-6 col-lg-12 secondary-image d-none d-xl-block" style="background-image:url('<?php echo ($metabox_content_array['whair_homepage_secondary_section_img']); ?>');">
      </div>
      <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 secondary-content">
        <div class="hero-wrapper-verticle">
          <div class="hero-wrapper-table" style="display: table-cell; vertical-align: middle;">
            <?php echo wpautop($metabox_content_array['whair_homepage_secondary_section_head_content']); ?>
            <hr align="left">
            <?php echo wpautop($metabox_content_array['whair_homepage_secondary_section_content']); ?>
            <a href="<?php echo ($metabox_content_array['whair_homepage_secondary_section_button_link']); ?>"><button class="btn btn-marg-top"><p><?php echo ($metabox_content_array['whair_homepage_secondary_section_button_text']); ?></p></button></a>
          </div>
        </div>
        <div class="test-wrap floating-img-right mouse-move-one d-none d-lg-block">
          <img src="<?php echo ($metabox_content_array['whair_homepage_secondary_section_floating_img']); ?>" class="img-fluid float-right fade-1s "/>
        </div>
      </div>
    </div>
  </div>

  <div class="tripple-cta-section">
    <div class="container">
      <div class="hero-wrapper-verticle">
        <div style="display: table-cell; vertical-align: middle;">
          <div class="row">
            <div class="col-lg-12">
              <?php echo wpautop($metabox_content_array['whair_homepage_tripple_cta_content']); ?>
              <hr align="left">
            </div>
            <div class="col-lg-4 col-md-12 col-sm-4 three-cta-col">
              <a href="<?php echo ($metabox_content_array['whair_homepage_cta_link_left']); ?>"><div class="img-hover"><img src="<?php echo ($metabox_content_array['whair_homepage_cta_image_left']); ?>" class="img-fluid mx-auto d-block"/><div class="overlay"></div></div></a>
            </div>
            <div class="col-lg-4 col-md-12 col-sm-4 three-cta-col">
              <a href="<?php echo ($metabox_content_array['whair_homepage_cta_link_center']); ?>"><div class="img-hover"><img src="<?php echo ($metabox_content_array['whair_homepage_cta_image_center']); ?>" class="img-fluid mx-auto d-block"/><div class="overlay"></div></div></a>
            </div>
            <div class="col-lg-4 col-md-12 col-sm-4 three-cta-col">
              <a href="<?php echo ($metabox_content_array['whair_homepage_cta_link_right']); ?>"><div class="img-hover"><img src="<?php echo ($metabox_content_array['whair_homepage_cta_image_right']); ?>" class="img-fluid mx-auto d-block"/><div class="overlay"></div></div></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php endwhile; ?>
