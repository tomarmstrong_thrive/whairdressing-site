  <div class="hero-wrapper" style="background-image:url('/wp-content/uploads/2017/12/homepage_hero.jpg');">
    <div class="hero-wrapper-verticle">
      <div style="display: table-cell; vertical-align: middle;">
        <div class="container">
          <div class="row">
            <div class="col-lg-8 mx-auto">
             <div class="hero-content">
               <h1>Sorry, but the page you were trying to view does not exist.</h1>
               <hr>
               <p><i>Please choose a page from the menu  below.<i></p>
             </div>
            </div>
          </div>
        </div>
        <div class="down-arrow fade-3s">
          <div id="scroll-down">
            <span class="arrow-down">
            <!-- css generated icon -->
            </span>
          </div>
        </div>
      </div>
    </div>
  </div>
