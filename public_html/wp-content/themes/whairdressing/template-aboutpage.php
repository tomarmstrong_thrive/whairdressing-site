<?php
/**
 * Template Name: About Page Template
 */
 $page_id = get_the_ID();

 $featured_image = wp_get_attachment_image_src(get_post_thumbnail_id($page_id), 'single-post-thumbnail');

 $metabox_id_array = array(
   'whair_aboutpage_secondary_section_head_content',
   'whair_aboutpage_secondary_section_content',
   'whair_aboutpage_floating_image_right',
   'whair_aboutpage_floating_image_left',
   'whair_aboutpage_final_content_section',
 );

 $metabox_content_array = get_metabox_content($page_id, $metabox_id_array);
 ?>

 <?php while (have_posts()) : the_post(); ?>
   <div class="hero-wrapper" style="background-image:url('<?php echo $featured_image[0] ?>');">
     <div class="hero-wrapper-verticle">
       <div style="display: table-cell; vertical-align: middle;">
         <div class="container">
           <div class="row">
             <div class="col-lg-8 mx-auto">
     		      <div class="hero-content"><?php echo the_content() ?></div>
             </div>
           </div>
         </div>
         <div class="down-arrow fade-3s">
           <div id="scroll-down">
             <span class="arrow-down">
             <!-- css generated icon -->
             </span>
           </div>
         </div>
       </div>
     </div>
   </div>

   <div class="about-main-section d-none d-xl-block">
     <div class="about-container-one">
       <div class="row">
         <div class="col-xl-6">
           <?php echo wpautop($metabox_content_array['whair_aboutpage_secondary_section_head_content']); ?>
           <hr align="left">
           <?php echo wpautop($metabox_content_array['whair_aboutpage_secondary_section_content']); ?>
         </div>
         <div class="col-xl-6">
           <img src="<?php echo ($metabox_content_array['whair_aboutpage_floating_image_right']); ?>" class="img-fluid float-right fade-1p5s"/>
         </div>
       </div>
     </div>

     <div class="about-container-two">
       <div class="row">
         <div class="col-xl-7">
           <img src="<?php echo ($metabox_content_array['whair_aboutpage_floating_image_left']); ?>" class="img-fluid float-left fade-2s"/>
         </div>
         <div class="col-xl-5">
           <hr align="left">
           <?php echo wpautop($metabox_content_array['whair_aboutpage_final_content_section']); ?>
         </div>
       </div>
     </div>
   </div>

   <div class="about-main-section d-xl-none">

     <div class="about-container-three">
       <div class="row">
         <div class="col-lg-12">
           <?php echo wpautop($metabox_content_array['whair_aboutpage_secondary_section_head_content']); ?>
           <hr align="left">
           <?php echo wpautop($metabox_content_array['whair_aboutpage_secondary_section_content']); ?>
         </div>
         <div class="col-lg-5 col-sm-5 floating-img-margin">
           <img src="<?php echo ($metabox_content_array['whair_aboutpage_floating_image_right']); ?>" class="img-fluid float-right fade-1p5s floating-img-left"/>
         </div>
         <div class="col-lg-7 col-sm-7">
           <img src="<?php echo ($metabox_content_array['whair_aboutpage_floating_image_left']); ?>" class="img-fluid float-left fade-2s floating-img-right"/>
         </div>
         <div class="col-lg-12">
           <hr align="left">
           <?php echo wpautop($metabox_content_array['whair_aboutpage_final_content_section']); ?>
         </div>
       </div>
     </div>
   </div>
<?php endwhile; ?>
