/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */
 // interval passed to reveal
 // or...

 // interval and custom config passed to reveal

 function contactDetailsColour(colour) {
     jQuery('.contact-details > h3').css('color', colour);
     jQuery('.contact-details > h4').css('color', colour);
 }

 function getNavWidth() {
     var width = jQuery(window).width();
     var navPercentage;

     if (width >= 1025) {
         navPercentage = "25%";
     } else if (width >= 401 ) {
         navPercentage = "50%";
     } else if (width <= 400 ) {
         navPercentage = "100%";
     }

     return navPercentage;
 }

 function openNav() {
      var navWidth = getNavWidth();
      document.getElementById("mySidenav").style.width = navWidth;
      jQuery('.burger-icon > img').attr("src","/wp-content/uploads/2017/12/close-icon.png");
      jQuery('#mySidenav').css('padding-right', "5%");
      jQuery('#mySidenav').css('padding-left', "3%");
      contactDetailsColour('black');
      jQuery('.burger-icon').click(function() {
          closeNav();
      });
  }

  function closeNav() {
      document.getElementById("mySidenav").style.width = "0";
      jQuery('#mySidenav').css('padding-right', "0%");
      jQuery('#mySidenav').css('padding-left', "0%");
      jQuery('.burger-icon > img').attr("src","/wp-content/uploads/2017/12/burger-icon.png");
      contactDetailsColour('white');

      jQuery('.burger-icon').click(function() {
          openNav();
      });
  }

(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function() {
        // JavaScript to be fired on all pages
      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
        jQuery('.burger-icon').click(function() {
            openNav();
        });
        window.sr = ScrollReveal();

        // Changing the defaults
        window.sr = ScrollReveal({ reset: true });

        // Customizing a reveal set
        sr.reveal('.fade-1s', { duration: 1000 , distance: '300px', });

        sr.reveal('.fade-1p5s', { duration: 1500 , distance: '300px', });

        sr.reveal('.fade-2s', { duration: 2000 , distance: '500px', });

        sr.reveal('.fade-3s', { duration: 3000 , distance: '300px', });

        sr.reveal('.fade-3p5s', { duration: 3750 , distance: '300px', });

        sr.reveal('.footer-one', { duration: 1500 , distance: '200px', });

        sr.reveal('.footer-two', { duration: 2000 , distance: '300px', });

        sr.reveal('.box', { duration: 2000 }, 500);

      }
    },
    // Home page
    'home': {
      init: function() {
        // JavaScript to be fired on the home page
      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // About us page, note the change from about-us to about_us.
    'about_us': {
      init: function() {
        // JavaScript to be fired on the about us page
      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
