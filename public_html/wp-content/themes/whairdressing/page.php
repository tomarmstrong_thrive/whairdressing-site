<?php

 $page_id = get_the_ID();

 $featured_image = wp_get_attachment_image_src(get_post_thumbnail_id($page_id), 'single-post-thumbnail');

 $metabox_content_array = get_metabox_content($page_id, $metabox_id_array);

?>

<?php while (have_posts()) : the_post(); ?>
  <div class="hero-wrapper" style="background-image:url('<?php echo $featured_image[0] ?>');">
    <div class="hero-wrapper-verticle">
      <div style="display: table-cell; vertical-align: middle;">
        <div class="container">
          <div class="row">
            <div class="col-lg-8 mx-auto">
             <div class="hero-content"><h1><?php echo the_title() ?></h1></div>
            </div>
          </div>
        </div>
        <div class="down-arrow fade-3s">
          <div id="scroll-down">
            <span class="arrow-down">
            <!-- css generated icon -->
            </span>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="generic-page-content-wrapper">
    <div class="container">
      <?php echo the_content() ?>
    </div>
  </div>
<?php endwhile; ?>
