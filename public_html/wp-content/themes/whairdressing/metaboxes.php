<?php
/**
 * Include and setup custom metaboxes and fields. (make sure you copy this file to outside the CMB2 directory)
 *
 * Be sure to replace all instances of 'yourprefix_' with your project's prefix.
 * http://nacin.com/2010/05/11/in-wordpress-prefix-everything/
 *
 * @category YourThemeOrPlugin
 * @package  Demo_CMB2
 * @license  http://www.opensource.org/licenses/gpl-license.php GPL v2.0 (or later)
 * @link     https://github.com/WebDevStudios/CMB2
 */

/**
 * Get the bootstrap! If using the plugin from wordpress.org, REMOVE THIS!
 */
//echo dirname( __FILE__ ).'/wp-content/plugins/cmb2/init.php';
//exit;

if ( file_exists( dirname( __FILE__ ) . '/wp-content/plugins/cmb2/init.php' ) ) {
	require_once dirname( __FILE__ ) . '/wp-content/plugins/cmb2/init.php';
} elseif ( file_exists( dirname( __FILE__ ) . '/wp-content/plugins/CMB2/init.php' ) ) {
	require_once dirname( __FILE__ ) . '/wp-content/plugins/CMB2/init.php';
}

/**
 * Conditionally displays a metabox when used as a callback in the 'show_on_cb' cmb2_box parameter
 *
 * @param  CMB2 object $cmb CMB2 object
 *
 * @return bool             True if metabox should show
 */
function whair_show_if_front_page( $cmb ) {
	// Don't show this metabox if it's not the front page template
	if ( $cmb->object_id !== get_option( 'page_on_front' ) ) {
		return false;
	}
	return true;
}

/**
 * Conditionally displays a field when used as a callback in the 'show_on_cb' field parameter
 *
 * @param  CMB2_Field object $field Field object
 *
 * @return bool                     True if metabox should show
 */
function whair_hide_if_no_cats( $field ) {
	// Don't show this field if not in the cats category
	if ( ! has_tag( 'cats', $field->object_id ) ) {
		return false;
	}
	return true;
}


// cta metabox component
//include("components/component-template/schema/metaboxes.php");


function whair_register_homepage_metabox() {
	$prefix = 'whair_homepage_';

	$cmb_homepage_page = new_cmb2_box( array(
		'id'           => $prefix . 'a_metabox',
		'title'        => __( 'Homepage Secondary Section', 'cmb2' ),
		'object_types' => array( 'page', ), // Post type
		'context'      => 'normal',
		'priority'     => 'high',
		'show_names'   => true, // Show field names on the left
		'show_on'      => array( 'key' => 'page-template', 'value' => array('template-homepage.php') ),
	) );

	$cmb_homepage_page->add_field( array(
		'name'    => 'Secondary Section Image',
		'id'      => $prefix.'secondary_section_img',
		'type'    => 'file',
	) );

	$cmb_homepage_page->add_field( array(
		'name'    			=> 'Secondary Section Header Content',
		'description'		=> __('Content entered here will appear below the main page image as a sub header.'),
		'id'      			=> $prefix.'secondary_section_head_content',
		'type'    			=> 'wysiwyg',
		'options' 			=> array(
	    'textarea_rows' => get_option('default_post_edit_rows', 5),
		),
	) );

	$cmb_homepage_page->add_field( array(
		'name'    			=> 'Secondary Section Content',
		'description'		=> __('Content entered here will appear above the button.'),
		'id'      			=> $prefix.'secondary_section_content',
		'type'    			=> 'wysiwyg',
		'options' 			=> array(
	    'textarea_rows' => get_option('default_post_edit_rows', 5),
		),
	) );

	$cmb_homepage_page->add_field( array(
		'name'    			=> 'Secondary Section Button Text',
		'description'		=> __('Enter the button text here.'),
		'id'      			=> $prefix.'secondary_section_button_text',
		'type'    			=> 'text',
	) );

	$cmb_homepage_page->add_field( array(
	  'name'    			=> 'Secondary Section Button Link',
		'description'		=> __('Enter the link for the location of this button here.'),
	  'id'      			=> $prefix.'secondary_section_button_link',
	  'type'    			=> 'text',
	) );

	$cmb_homepage_page->add_field( array(
		'name'    			=> 'Secondary Section Floating Image',
		'description'		=> __('This image will apear under the content and button.'),
		'id'      			=> $prefix.'secondary_section_floating_img',
		'type'    			=> 'file',
	) );


	$cmb_homepage_page = new_cmb2_box( array(
		'id'           => $prefix . 'b_metabox',
		'title'        => __( 'Homepage Tripple CTA Section', 'cmb2' ),
		'object_types' => array( 'page', ), // Post type
		'context'      => 'normal',
		'priority'     => 'high',
		'show_names'   => true, // Show field names on the left
		'show_on'      => array( 'key' => 'page-template', 'value' => array('template-homepage.php') ),
	) );

	$cmb_homepage_page->add_field( array(
		'name'    			=> 'Tripple CTA Section Content',
		'description'		=> __('Content entered here will appear above the three CTAs.'),
		'id'      			=> $prefix.'tripple_cta_content',
		'type'    			=> 'wysiwyg',
		'options' 			=> array(
			'textarea_rows' => get_option('default_post_edit_rows', 5),
		),
	) );

	$cmb_homepage_page->add_field( array(
		'name'    			=> 'CTA Left Image',
		'description'		=> __('This image will apear as the first and left CTA Background.'),
		'id'      			=> $prefix.'cta_image_left',
		'type'    			=> 'file',
	) );

	$cmb_homepage_page->add_field( array(
		'name'    			=> 'CTA Left Link',
		'description'		=> __('Enter the link for the location of this CTA here.'),
		'id'      			=> $prefix.'cta_link_left',
		'type'    			=> 'text',
	) );

	$cmb_homepage_page->add_field( array(
		'name'    			=> 'CTA Center Image',
		'description'		=> __('This image will apear as the second and center CTA Background.'),
		'id'      			=> $prefix.'cta_image_center',
		'type'    			=> 'file',
	) );

	$cmb_homepage_page->add_field( array(
		'name'    			=> 'CTA Center Link',
		'description'		=> __('Enter the link for the location of this CTA here.'),
		'id'      			=> $prefix.'cta_link_center',
		'type'    			=> 'text',
	) );

	$cmb_homepage_page->add_field( array(
		'name'    			=> 'CTA Right Image',
		'description'		=> __('This image will apear as the third and right CTA Background.'),
		'id'      			=> $prefix.'cta_image_right',
		'type'    			=> 'file',
	) );

	$cmb_homepage_page->add_field( array(
		'name'    			=> 'CTA Right Link',
		'description'		=> __('Enter the link for the location of this CTA here.'),
		'id'      			=> $prefix.'cta_link_right',
		'type'    			=> 'text',
	) );

}

add_action( 'cmb2_admin_init', 'whair_register_homepage_metabox' );



function whair_register_aboutpage_metabox() {
	$prefix = 'whair_aboutpage_';

	$cmb_aboutpage_page = new_cmb2_box( array(
		'id'           => $prefix . 'a_metabox',
		'title'        => __( 'About Page Secondary Section', 'cmb2' ),
		'object_types' => array( 'page', ), // Post type
		'context'      => 'normal',
		'priority'     => 'high',
		'show_names'   => true, // Show field names on the left
		'show_on'      => array( 'key' => 'page-template', 'value' => array('template-aboutpage.php') ),
	) );

	$cmb_aboutpage_page->add_field( array(
		'name'    			=> 'Secondary Section Header Content',
		'description'		=> __('Content entered here will appear below the main page image as a sub header.'),
		'id'      			=> $prefix.'secondary_section_head_content',
		'type'    			=> 'wysiwyg',
		'options' 			=> array(
			'textarea_rows' => get_option('default_post_edit_rows', 5),
		),
	) );

	$cmb_aboutpage_page->add_field( array(
		'name'    			=> 'Secondary Section Content',
		'description'		=> __('Content entered here will appear below the sub header.'),
		'id'      			=> $prefix.'secondary_section_content',
		'type'    			=> 'wysiwyg',
		'options' 			=> array(
			'textarea_rows' => get_option('default_post_edit_rows', 5),
		),
	) );

	$cmb_aboutpage_page->add_field( array(
		'name'    			=> 'Floating Image One (Right)',
		'description'		=> __('Upload an image here to appear as the floating page image on the right of the page.'),
		'id'      			=> $prefix.'floating_image_right',
		'type'    			=> 'file',
	) );

	$cmb_aboutpage_page->add_field( array(
		'name'    			=> 'Floating Image One (Left)',
		'description'		=> __('Upload an image here to appear as the floating page image on the left of the page.'),
		'id'      			=> $prefix.'floating_image_left',
		'type'    			=> 'file',
	) );

	$cmb_aboutpage_page->add_field( array(
		'name'    			=> 'Final Content Section',
		'description'		=> __('Content entered here will appear as the final content block on this page.'),
		'id'      			=> $prefix.'final_content_section',
		'type'    			=> 'wysiwyg',
		'options' 			=> array(
	    'textarea_rows' => get_option('default_post_edit_rows', 5),
		),
	) );

}

add_action( 'cmb2_admin_init', 'whair_register_aboutpage_metabox' );


function whair_register_servicespage_metabox() {
	$prefix = 'whair_servicespage_';

	$cmb_servicespage_page = new_cmb2_box( array(
		'id'           => $prefix . 'a_metabox',
		'title'        => __( 'Services Page Secondary Section', 'cmb2' ),
		'object_types' => array( 'page', ), // Post type
		'context'      => 'normal',
		'priority'     => 'high',
		'show_names'   => true, // Show field names on the left
		'show_on'      => array( 'key' => 'page-template', 'value' => array('template-servicespage.php') ),
	) );

	$cmb_servicespage_page->add_field( array(
		'name'    			=> 'Secondary Section Header Content',
		'description'		=> __('Content entered here will appear below the main page image as a sub header.'),
		'id'      			=> $prefix.'secondary_section_head_content',
		'type'    			=> 'wysiwyg',
		'options' 			=> array(
			'textarea_rows' => get_option('default_post_edit_rows', 5),
		),
	) );

	$cmb_servicespage_page->add_field( array(
		'name'    			=> 'Secondary Section Content',
		'description'		=> __('Content entered here will appear below the sub header.'),
		'id'      			=> $prefix.'secondary_section_content',
		'type'    			=> 'wysiwyg',
		'options' 			=> array(
			'textarea_rows' => get_option('default_post_edit_rows', 5),
		),
	) );

	$cmb_servicespage_page->add_field( array(
		'name'    			=> 'Secondary Section Image',
		'description'		=> __('Upload an image here to appear as the Secondary Section Image.'),
		'id'      			=> $prefix.'secondary_section_image',
		'type'    			=> 'file',
	) );
}

add_action( 'cmb2_admin_init', 'whair_register_servicespage_metabox' );


function whair_register_pricingpage_metabox() {
	$prefix = 'whair_pricingpage_';


	///
	$cmb_pricingpage_page = new_cmb2_box( array(
		'id'           => $prefix . 'a_metabox',
		'title'        => __( 'Pricing Table One', 'cmb2' ),
		'object_types' => array( 'page', ), // Post type
		'context'      => 'normal',
		'priority'     => 'high',
		'show_names'   => true, // Show field names on the left
		'show_on'      => array( 'key' => 'page-template', 'value' => array('template-pricingpage.php') ),
	) );

	$cmb_pricingpage_page->add_field( array(
		'name'    			=> 'Pricing Table One Title',
		'description'		=> __('Enter the Pricing Table Title here.'),
		'id'      			=> $prefix.'pricing_table_one_title',
		'type'    			=> 'text',
	) );

	$pricing_tables_group_id = $cmb_pricingpage_page->add_field( array(
		'id'          => $prefix.'pricing_table_one',
		'type'        => 'group',
		'description' => __( 'Enter a title and a table within the content section to create a finished Pricing Table section.', 'cmb2' ),
		// 'repeatable'  => false, // use false if you want non-repeatable group
		'options'     => array(
				'group_title'   => __( 'Pricing Table One Entry: {#}', 'cmb2' ), // since version 1.1.4, {#} gets replaced by row number
				'add_button'    => __( 'Add Another Entry', 'cmb2' ),
				'remove_button' => __( 'Remove Entry', 'cmb2' ),
				'sortable'      => true, // beta
		),
		) );

		$cmb_pricingpage_page->add_group_field($pricing_tables_group_id, array(
			'name'    			=> 'Service Name',
			'description'		=> __('Enter the service name here, for e.g Full Head.'),
			'id'      			=> $prefix.'pricing_table_item',
			'type'    			=> 'text',
		) );

		$cmb_pricingpage_page->add_group_field($pricing_tables_group_id, array(
			'name'    			=> 'Service Price',
			'description'		=> __('Enter the price of the service here.'),
			'id'      			=> $prefix.'pricing_table_price',
			'type'    			=> 'text',
		) );
		///

	///
	$cmb_pricingpage_page = new_cmb2_box( array(
		'id'           => $prefix . 'b_metabox',
		'title'        => __( 'Pricing Table Two', 'cmb2' ),
		'object_types' => array( 'page', ), // Post type
		'context'      => 'normal',
		'priority'     => 'high',
		'show_names'   => true, // Show field names on the left
		'show_on'      => array( 'key' => 'page-template', 'value' => array('template-pricingpage.php') ),
	) );

	$cmb_pricingpage_page->add_field( array(
		'name'    			=> 'Pricing Table Two Title',
		'description'		=> __('Enter the Pricing Table Title here.'),
		'id'      			=> $prefix.'pricing_table_two_title',
		'type'    			=> 'text',
	) );

	$pricing_tables_group_id = $cmb_pricingpage_page->add_field( array(
		'id'          => $prefix.'pricing_table_two',
		'type'        => 'group',
		'description' => __( 'Enter a title and a table within the content section to create a finished Pricing Table section.', 'cmb2' ),
		// 'repeatable'  => false, // use false if you want non-repeatable group
		'options'     => array(
				'group_title'   => __( 'Pricing Table Two Entry: {#}', 'cmb2' ), // since version 1.1.4, {#} gets replaced by row number
				'add_button'    => __( 'Add Another Entry', 'cmb2' ),
				'remove_button' => __( 'Remove Entry', 'cmb2' ),
				'sortable'      => true, // beta
		),
		) );

		$cmb_pricingpage_page->add_group_field($pricing_tables_group_id, array(
			'name'    			=> 'Service Name',
			'description'		=> __('Enter the service name here, for e.g Full Head.'),
			'id'      			=> $prefix.'pricing_table_item',
			'type'    			=> 'text',
		) );

		$cmb_pricingpage_page->add_group_field($pricing_tables_group_id, array(
			'name'    			=> 'Service Price',
			'description'		=> __('Enter the price of the service here.'),
			'id'      			=> $prefix.'pricing_table_price',
			'type'    			=> 'text',
		) );
		///

	///
	$cmb_pricingpage_page = new_cmb2_box( array(
		'id'           => $prefix . 'c_metabox',
		'title'        => __( 'Pricing Table Three', 'cmb2' ),
		'object_types' => array( 'page', ), // Post type
		'context'      => 'normal',
		'priority'     => 'high',
		'show_names'   => true, // Show field names on the left
		'show_on'      => array( 'key' => 'page-template', 'value' => array('template-pricingpage.php') ),
	) );

	$cmb_pricingpage_page->add_field( array(
		'name'    			=> 'Pricing Table Three Title',
		'description'		=> __('Enter the Pricing Table Title here.'),
		'id'      			=> $prefix.'pricing_table_three_title',
		'type'    			=> 'text',
	) );

	$pricing_tables_group_id = $cmb_pricingpage_page->add_field( array(
		'id'          => $prefix.'pricing_table_three',
		'type'        => 'group',
		'description' => __( 'Enter a title and a table within the content section to create a finished Pricing Table section.', 'cmb2' ),
		// 'repeatable'  => false, // use false if you want non-repeatable group
		'options'     => array(
				'group_title'   => __( 'Pricing Table Three Entry: {#}', 'cmb2' ), // since version 1.1.4, {#} gets replaced by row number
				'add_button'    => __( 'Add Another Entry', 'cmb2' ),
				'remove_button' => __( 'Remove Entry', 'cmb2' ),
				'sortable'      => true, // beta
		),
		) );

			$cmb_pricingpage_page->add_group_field($pricing_tables_group_id, array(
				'name'    			=> 'Service Name',
				'description'		=> __('Enter the service name here, for e.g Full Head.'),
				'id'      			=> $prefix.'pricing_table_item',
				'type'    			=> 'text',
			) );

			$cmb_pricingpage_page->add_group_field($pricing_tables_group_id, array(
				'name'    			=> 'Service Price',
				'description'		=> __('Enter the price of the service here.'),
				'id'      			=> $prefix.'pricing_table_price',
				'type'    			=> 'text',
			) );


		$cmb_pricingpage_page->add_field( array(
			'name'    			=> 'Pricing Table Three Additional Content',
			'description'		=> __('Enter Additional Content here.'),
			'id'      			=> $prefix.'pricing_table_three_ac',
			'type'    			=> 'wysiwyg',
			'options' 			=> array(
				'textarea_rows' => get_option('default_post_edit_rows', 5),
			),
		) );
		///

	///
	$cmb_pricingpage_page = new_cmb2_box( array(
		'id'           => $prefix . 'd_metabox',
		'title'        => __( 'Pricing Table Four', 'cmb2' ),
		'object_types' => array( 'page', ), // Post type
		'context'      => 'normal',
		'priority'     => 'high',
		'show_names'   => true, // Show field names on the left
		'show_on'      => array( 'key' => 'page-template', 'value' => array('template-pricingpage.php') ),
	) );

	$cmb_pricingpage_page->add_field( array(
		'name'    			=> 'Pricing Table Four Title',
		'description'		=> __('Enter the Pricing Table Title here.'),
		'id'      			=> $prefix.'pricing_table_four_title',
		'type'    			=> 'text',
	) );

	$pricing_tables_group_id = $cmb_pricingpage_page->add_field( array(
		'id'          => $prefix.'pricing_table_four',
		'type'        => 'group',
		'description' => __( 'Enter a title and a table within the content section to create a finished Pricing Table section.', 'cmb2' ),
		// 'repeatable'  => false, // use false if you want non-repeatable group
		'options'     => array(
				'group_title'   => __( 'Pricing Table Four Entry: {#}', 'cmb2' ), // since version 1.1.4, {#} gets replaced by row number
				'add_button'    => __( 'Add Another Entry', 'cmb2' ),
				'remove_button' => __( 'Remove Entry', 'cmb2' ),
				'sortable'      => true, // beta
		),
		) );

			$cmb_pricingpage_page->add_group_field($pricing_tables_group_id, array(
				'name'    			=> 'Service Name',
				'description'		=> __('Enter the service name here, for e.g Full Head.'),
				'id'      			=> $prefix.'pricing_table_item',
				'type'    			=> 'text',
			) );

			$cmb_pricingpage_page->add_group_field($pricing_tables_group_id, array(
				'name'    			=> 'Service Price',
				'description'		=> __('Enter the price of the service here.'),
				'id'      			=> $prefix.'pricing_table_price',
				'type'    			=> 'text',
			) );
			///

	//
	$cmb_pricingpage_page = new_cmb2_box( array(
		'id'           => $prefix . 'e_metabox',
		'title'        => __( 'Pricing Table Five', 'cmb2' ),
		'object_types' => array( 'page', ), // Post type
		'context'      => 'normal',
		'priority'     => 'high',
		'show_names'   => true, // Show field names on the left
		'show_on'      => array( 'key' => 'page-template', 'value' => array('template-pricingpage.php') ),
	) );

	$cmb_pricingpage_page->add_field( array(
		'name'    			=> 'Pricing Table Five Title',
		'description'		=> __('Enter the Pricing Table Title here.'),
		'id'      			=> $prefix.'pricing_table_five_title',
		'type'    			=> 'text',
	) );

	$pricing_tables_group_id = $cmb_pricingpage_page->add_field( array(
		'id'          => $prefix.'pricing_table_five',
		'type'        => 'group',
		'description' => __( 'Enter a title and a table within the content section to create a finished Pricing Table section.', 'cmb2' ),
		// 'repeatable'  => false, // use false if you want non-repeatable group
		'options'     => array(
				'group_title'   => __( 'Pricing Table Five Entry: {#}', 'cmb2' ), // since version 1.1.4, {#} gets replaced by row number
				'add_button'    => __( 'Add Another Entry', 'cmb2' ),
				'remove_button' => __( 'Remove Entry', 'cmb2' ),
				'sortable'      => true, // beta
		),
		) );

				$cmb_pricingpage_page->add_group_field($pricing_tables_group_id, array(
					'name'    			=> 'Service Name',
					'description'		=> __('Enter the service name here, for e.g Full Head.'),
					'id'      			=> $prefix.'pricing_table_item',
					'type'    			=> 'text',
				) );

				$cmb_pricingpage_page->add_group_field($pricing_tables_group_id, array(
					'name'    			=> 'Service Price',
					'description'		=> __('Enter the price of the service here.'),
					'id'      			=> $prefix.'pricing_table_price',
					'type'    			=> 'text',
				) );


	$cmb_pricingpage_page = new_cmb2_box( array(
		'id'           => $prefix . 'f_metabox',
		'title'        => __( 'Pricing Page Floating Images Section', 'cmb2' ),
		'object_types' => array( 'page', ), // Post type
		'context'      => 'normal',
		'priority'     => 'high',
		'show_names'   => true, // Show field names on the left
		'show_on'      => array( 'key' => 'page-template', 'value' => array('template-pricingpage.php') ),
	) );

	$cmb_pricingpage_page->add_field( array(
		'name'    			=> 'Floating Image Top',
		'description'		=> __('Upload an image for top Floating Image'),
		'id'      			=> $prefix.'floating_img_top',
		'type'    			=> 'file',
	) );

	$cmb_pricingpage_page->add_field( array(
		'name'    			=> 'Floating Image Bottom',
		'description'		=> __('Upload an image for bottom Floating Image'),
		'id'      			=> $prefix.'floating_img_bottom',
		'type'    			=> 'file',
	) );

	$cmb_pricingpage_page = new_cmb2_box( array(
		'id'           => $prefix . 'g_metabox',
		'title'        => __( 'Pricing Page Meet the Team CTA', 'cmb2' ),
		'object_types' => array( 'page', ), // Post type
		'context'      => 'normal',
		'priority'     => 'high',
		'show_names'   => true, // Show field names on the left
		'show_on'      => array( 'key' => 'page-template', 'value' => array('template-pricingpage.php') ),
	) );

	$cmb_pricingpage_page->add_field( array(
		'name'    			=> 'Meet the Team CTA Title',
		'description'		=> __('Enter the title for the Meet the Team CTA section here.'),
		'id'      			=> $prefix.'team_cta_title',
		'type'    			=> 'text',
	) );

	$cmb_pricingpage_page->add_field( array(
		'name'    			=> 'Meet the Team CTA Content',
		'description'		=> __('Enter the content for the Meet the Team CTA section here.'),
		'id'      			=> $prefix.'team_cta_content',
		'type'    			=> 'wysiwyg',
		'options' 			=> array(
			'textarea_rows' => get_option('default_post_edit_rows', 5),
		),
	) );

	$cmb_pricingpage_page->add_field( array(
		'name'    			=> 'Meet the Team CTA Button Text',
		'description'		=> __('Enter the Meet the Team CTA Button Text'),
		'id'      			=> $prefix.'team_cta_button_text',
		'type'    			=> 'text',
	) );

	$cmb_pricingpage_page->add_field( array(
		'name'    			=> 'Meet the Team CTA Button Link',
		'description'		=> __('Enter the Meet the Team CTA Button Link'),
		'id'      			=> $prefix.'team_cta_button_link',
		'type'    			=> 'text',
	) );

	$cmb_pricingpage_page->add_field( array(
		'name'    			=> 'Meet the Team CTA Floating Image',
		'description'		=> __('Upload an image for the Meet the Team CTA section herek'),
		'id'      			=> $prefix.'team_cta_image',
		'type'    			=> 'file',
	) );
}

add_action( 'cmb2_admin_init', 'whair_register_pricingpage_metabox' );


function whair_register_contactpage_metabox() {
	$prefix = 'whair_contactpage_';

	$cmb_contactpage_page = new_cmb2_box( array(
		'id'           => $prefix . 'a_metabox',
		'title'        => __( 'Contact page Secondary Section', 'cmb2' ),
		'object_types' => array( 'page', ), // Post type
		'context'      => 'normal',
		'priority'     => 'high',
		'show_names'   => true, // Show field names on the left
		'show_on'      => array( 'key' => 'page-template', 'value' => array('template-contactpage.php') ),
	) );

	$cmb_contactpage_page->add_field( array(
		'name'    			=> 'Contact Information Content',
		'description'		=> __('Content entered here will appear above the contact form as a sub header.'),
		'id'      			=> $prefix.'contact_content',
		'type'    			=> 'wysiwyg',
		'options' 			=> array(
	    'textarea_rows' => get_option('default_post_edit_rows', 5),
		),
	) );

	$cmb_contactpage_page->add_field( array(
		'name'    => 'Contact Information Floating Image',
		'id'      => $prefix.'contact_floating_img',
		'type'    => 'file',
	) );


	$cmb_contactpage_page = new_cmb2_box( array(
		'id'           => $prefix . 'b_metabox',
		'title'        => __( 'Contact page Tripple CTA Section', 'cmb2' ),
		'object_types' => array( 'page', ), // Post type
		'context'      => 'normal',
		'priority'     => 'high',
		'show_names'   => true, // Show field names on the left
		'show_on'      => array( 'key' => 'page-template', 'value' => array('template-contactpage.php') ),
	) );

	$cmb_contactpage_page->add_field( array(
		'name'    			=> 'Tripple CTA Section Content',
		'description'		=> __('Content entered here will appear above the three CTAs.'),
		'id'      			=> $prefix.'tripple_cta_content',
		'type'    			=> 'wysiwyg',
		'options' 			=> array(
			'textarea_rows' => get_option('default_post_edit_rows', 5),
		),
	) );

	$cmb_contactpage_page->add_field( array(
		'name'    			=> 'CTA Left Image',
		'description'		=> __('This image will apear as the first and left CTA Background.'),
		'id'      			=> $prefix.'cta_image_left',
		'type'    			=> 'file',
	) );

	$cmb_contactpage_page->add_field( array(
		'name'    			=> 'CTA Left Link',
		'description'		=> __('Enter the link for the location of this CTA here.'),
		'id'      			=> $prefix.'cta_link_left',
		'type'    			=> 'text',
	) );

	$cmb_contactpage_page->add_field( array(
		'name'    			=> 'CTA Center Image',
		'description'		=> __('This image will apear as the second and center CTA Background.'),
		'id'      			=> $prefix.'cta_image_center',
		'type'    			=> 'file',
	) );

	$cmb_contactpage_page->add_field( array(
		'name'    			=> 'CTA Center Link',
		'description'		=> __('Enter the link for the location of this CTA here.'),
		'id'      			=> $prefix.'cta_link_center',
		'type'    			=> 'text',
	) );

	$cmb_contactpage_page->add_field( array(
		'name'    			=> 'CTA Right Image',
		'description'		=> __('This image will apear as the third and right CTA Background.'),
		'id'      			=> $prefix.'cta_image_right',
		'type'    			=> 'file',
	) );

	$cmb_contactpage_page->add_field( array(
		'name'    			=> 'CTA Right Link',
		'description'		=> __('Enter the link for the location of this CTA here.'),
		'id'      			=> $prefix.'cta_link_right',
		'type'    			=> 'text',
	) );

}

add_action( 'cmb2_admin_init', 'whair_register_contactpage_metabox' );





/***** ALL PAGES *****/
function whair_register_general_metabox() {

	// Start with an underscore to hide fields from custom fields list
	$prefix = 'whair_general_';

	/**
	 * Metabox to be displayed on a single page ID
	 */
	$cmb_general = new_cmb2_box( array(
		'id'           => $prefix . 'z_metabox',
		'title'        => __( 'Footer CTA Section', 'cmb2' ),
		'object_types' => array( 'page'), // Post type
		'context'      => 'normal',
		'priority'     => 'high',
		'show_names'   => true, // Show field names on the left
		//'show_on'      => array( 'key' => 'page-template', 'value' => 'template-homepage.php' ),//
	) );

	$cmb_general->add_field( array(
	    'name'       => 'Footer CTA Section Display Options',
	    'desc' 			 => 'Check the box to hide the Footer CTA Section on this page',
	    'id'				 => 'footer_cta_display',
	    'type'			 => 'checkbox'
	) );

	$cmb_general->add_field( array(
		'name'    			=> 'Footer CTA Title',
		'description'		=> __('Enter the title for the Footer CTA here.'),
		'id'      			=> $prefix.'footer_cta_title',
		'type'    			=> 'text',
	) );

	$cmb_general->add_field( array(
		'name'    			=> 'Footer CTA Content',
		'description'		=> __('Enter the content for the Footer CTA here.'),
		'id'      			=> $prefix.'footer_cta_content',
		'type'    			=> 'wysiwyg',
		'options' 			=> array(
			'textarea_rows' => get_option('default_post_edit_rows', 5),
		),
	) );

	$cmb_general->add_field( array(
		'name'    			=> 'Footer CTA Button Text',
		'description'		=> __('Enter the Button Text for the Footer CTA here.'),
		'id'      			=> $prefix.'footer_cta_button_text',
		'type'    			=> 'text',
	) );

	$cmb_general->add_field( array(
		'name'    			=> 'Footer CTA Button Link',
		'description'		=> __('Enter the Button Link for the Footer CTA here.'),
		'id'      			=> $prefix.'footer_cta_button_link',
		'type'    			=> 'text',
	) );

	$cmb_general->add_field( array(
		'name'    			=> 'Footer CTA Image One',
		'description'		=> __('Upload an image here, this image will appear more to the left of the page.'),
		'id'      			=> $prefix.'footer_cta_image_one',
		'type'    			=> 'file',
	) );

	$cmb_general->add_field( array(
		'name'    			=> 'Footer CTA Image Two',
		'description'		=> __('Upload an image here, this image will appear to the right or next to the first image.'),
		'id'      			=> $prefix.'footer_cta_image_two',
		'type'    			=> 'file',
	) );
}

add_action( 'cmb2_init', 'whair_register_general_metabox' );
/***** ALL PAGES *****/
