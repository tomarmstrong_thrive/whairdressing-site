<?php

$page_id = get_the_ID();

$metabox_id_array = array(
  'whair_general_footer_cta_title',
  'whair_general_footer_cta_content',
  'whair_general_footer_cta_button_text',
  'whair_general_footer_cta_button_link',
  'whair_general_footer_cta_image_one',
  'whair_general_footer_cta_image_two',
);

$metabox_content_array = get_metabox_content($page_id, $metabox_id_array);
?>
<footer class="footer-wrapper">
<?php $footer_cta_display = get_post_meta(get_the_ID(), 'footer_cta_display', true); ?>
<?php if($footer_cta_display !== "on"){ ?>
  <div class="footer-cta-section">
    <div class="footer-img-wrapper floating-img-one mouse-move-two d-none d-lg-block">
      <img src="<?php echo ($metabox_content_array['whair_general_footer_cta_image_one']); ?>" class="img-fluid float-right footer-one"/>
    </div>
    <div class="footer-img-wrapper floating-img-two mouse-move-three d-none d-lg-block">
      <img src="<?php echo ($metabox_content_array['whair_general_footer_cta_image_two']); ?>" class="img-fluid float-right footer-two"/>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 mx-auto">
          <h2><?php echo ($metabox_content_array['whair_general_footer_cta_title']); ?></h2>
          <hr>
          <?php echo wpautop($metabox_content_array['whair_general_footer_cta_content']); ?>
          <a href="<?php echo ($metabox_content_array['whair_general_footer_cta_button_link']); ?>"><button data-toggle="modal" data-target="<?php echo ($metabox_content_array['whair_general_footer_cta_button_link']); ?>"class="btn btn-marg-top"><p><?php echo ($metabox_content_array['whair_general_footer_cta_button_text']); ?></p></button></a>
        </div>
      </div>
    </div>
  </div>
<?php } ?>

  <div class="footer-logos-wrapper">
    <div class="container">
      <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-3 col-6">
          <div class="hero-wrapper-verticle">
            <div style="display: table-cell; vertical-align: middle;">
              <img src="/wp-content/uploads/2017/12/wella-logo.png" alt="Wella Logo" class="img-fluid float-left">
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-3 col-6">
          <div class="hero-wrapper-verticle">
            <div style="display: table-cell; vertical-align: middle;">
              <img src="/wp-content/uploads/2017/12/sp-logo.png" alt="SP Logo" class="img-fluid mx-auto d-block">
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-3 col-6">
          <div class="hero-wrapper-verticle">
            <div style="display: table-cell; vertical-align: middle;">
              <img src="/wp-content/uploads/2017/12/eimi-logo.png" alt="EIMI Logo" class="img-fluid mx-auto d-block">
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-3 col-6">
          <div class="hero-wrapper-verticle">
            <div style="display: table-cell; vertical-align: middle;">
              <img src="/wp-content/uploads/2017/12/sebastian-logo.png" alt="Proffessional Sebastian Logo" class="img-fluid float-right">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="footer-main-wrapper d-none d-lg-block d-xl-block">
    <div class="container">
      <div class="row">
        <div class="col-lg-3 footer-low-colum">
          <nav class="nav-footer">
            <?php
            if (has_nav_menu('footer_navigation')) :
              wp_nav_menu(['theme_location' => 'footer_navigation', 'menu_class' => 'nav']);
            endif;
            ?>
            <ul id="mega-menu-footer_navigation_vacancies" class="mega-menu mega-menu-horizontal" data-event="hover_intent" data-effect="fade_up" data-effect-speed="200" data-second-click="close" data-document-click="collapse" data-vertical-behaviour="standard" data-breakpoint="1024" data-unbind="true">
              <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout mega-menu-item-70" id="mega-menu-item-70">
                <a class="mega-menu-link vacancies" data-toggle="modal" data-target="#myModal" style="cursor:pointer">
                  Vacancies
                </a>
              </li>
            </ul>
          </nav>
        </div>
        <div class="col-lg-6">
          <a href="<?= esc_url(home_url('/')); ?>"><img src="/wp-content/uploads/2017/12/w-hairdressing_logo.png" alt="W HairdressingLogo" class="img-fluid mx-auto d-block"></a>
          <div class="newsletter-wrapper">
              <?php echo do_shortcode('[contact-form-7 id="44" title="Newsletter"]') ?>
          </div>
        </div>
        <div class="col-lg-3 footer-low-colum">
          <div class="sidebar-content"><?php dynamic_sidebar('sidebar-footer'); ?>
            <div class="social-icons">
              <a href="https://www.facebook.com/WHairBeauty/" target="_blank"><i class="fa fa-2x fa-facebook-official" aria-hidden="true"></i></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="footer-main-wrapper d-block d-xl-none d-lg-none">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 col-md-12">
          <a href="<?= esc_url(home_url('/')); ?>"><img src="/wp-content/uploads/2017/12/w-hairdressing_logo.png" alt="W HairdressingLogo" class="img-fluid mx-auto d-block"></a>
          <div class="newsletter-wrapper">
              <?php echo do_shortcode('[contact-form-7 id="44" title="Newsletter"]') ?>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 footer-low-colum">
          <nav class="nav-footer">
            <?php
            if (has_nav_menu('footer_navigation')) :
              wp_nav_menu(['theme_location' => 'footer_navigation', 'menu_class' => 'nav']);
            endif;
            ?>
            <ul id="mega-menu-footer_navigation_vacancies" class="mega-menu mega-menu-horizontal" data-event="hover_intent" data-effect="fade_up" data-effect-speed="200" data-second-click="close" data-document-click="collapse" data-vertical-behaviour="standard" data-breakpoint="1024" data-unbind="true">
              <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout mega-menu-item-70" id="mega-menu-item-70">
                <a class="mega-menu-link vacancies" data-toggle="modal" data-target="#myModal" style="cursor:pointer">
                  Vacancies
                </a>
              </li>
            </ul>
          </nav>
        </div>
        <div class="col-lg-3 col-md-6 footer-low-colum">
          <div class="sidebar-content"><?php dynamic_sidebar('sidebar-footer'); ?>
            <div class="social-icons">
              <a href="https://www.facebook.com/WHairBeauty/" tagert="_blank"><i class="fa fa-2x fa-facebook-official" aria-hidden="true"></i></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <hr>

  <div class="footer-bottom-wrapper">
    <div class="container">
      <div class="row">
        <div class="col-lg-3">
        </div>
        <div class="col-lg-6">
          <p>Copyright © <?php echo date('Y'); ?> W Hairdressing</p>
        </div>
        <div class="col-lg-3">
        </div>
      </div>
    </div>
  </div>

  <!-- Modal -->
  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <?php echo do_shortcode('[contact-form-7 id="146" title="Send us your CV Form"]') ?>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

  <script>
  		$( document ).mousemove( function( e ) {
  			$( '.mouse-move-one' )	  .parallax( -30	, e );
        $( '.mouse-move-two' ).parallax( -30, e );
  			$( '.mouse-move-three' )	  .parallax( 30	, e );
  		});
  	</script>
</footer>
