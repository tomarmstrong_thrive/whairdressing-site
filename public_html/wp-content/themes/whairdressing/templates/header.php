<header class="banner">
  <div id="mySidenav" class="sidenav">
    <nav class="nav-primary">
      <?php
      if (has_nav_menu('primary_navigation')) :
        wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']);
      endif;
      ?>
    </nav>
    <div class="row">
      <div class="col-lg-6">
        <hr align="left">
      </div>
      <div class="col-lg-6">
        <div class="social-icon">
          <a href="https://www.facebook.com/WHairBeauty/" target="_blank"><i class="fa fa-2x fa-facebook-official" aria-hidden="true"></i></a>
        </div>
      </div>
    </div>
  </div>

  <div class="d-none d-sm-block">
    <div class="row">
      <div class="col-lg-4 col-md-4 col-sm-4">
        <p><a href="tel:01325485444">T. 01325 485 444</a></p>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-4">
        <a href="<?= esc_url(home_url('/')); ?>"><img src="/wp-content/uploads/2017/12/w-hairdressing_logo_header.png" alt="W HairdressingLogo" class="img-fluid mx-auto d-block"></a>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-4">
        <div class="burger-icon float-right">
            <img src="/wp-content/uploads/2017/12/burger-icon.png" alt="Burger Open Icon">
        </div>
      </div>
    </div>
  </div>

  <div class="d-block d-sm-none">
    <div class="row">
      <div class="col-2">
      </div>
      <div class="col-8">
        <a href="<?= esc_url(home_url('/')); ?>"><img src="/wp-content/uploads/2017/12/w-hairdressing_logo_header.png" alt="W HairdressingLogo" class="img-fluid mx-auto d-block"></a>
      </div>
      <div class="col-2">
        <div class="burger-icon float-right">
            <img src="/wp-content/uploads/2017/12/burger-icon.png" alt="Burger Open Icon">
        </div>
      </div>
    </div>
  </div>
</header>
