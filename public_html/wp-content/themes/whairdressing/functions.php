<?php
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
  'lib/assets.php',    // Scripts and stylesheets
  'lib/extras.php',    // Custom functions
  'lib/setup.php',     // Theme setup
  'lib/titles.php',    // Page titles
  'lib/wrapper.php',   // Theme wrapper class
  'lib/customizer.php', // Theme customizer
  'metaboxes.php', // Theme customizer
];

foreach ($sage_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);

/* This takes a list of all the metabox id's you need content
   for and pushes it into associative single array. */
function get_metabox_content($page_id, $metabox_id_array)
{
    $metabox_content_array = array();

    foreach ($metabox_id_array as $metabox_id)
    {
        $metabox_content = get_post_meta($page_id, $metabox_id, true);
        $metabox_content_array[$metabox_id] = $metabox_content;
    }

    return $metabox_content_array;
}

function draw_pricing_table_one($draw_pricing_table_one) {
    $html = '';

    foreach ($draw_pricing_table_one as $section) {
      $html .= '<table>';
        $html .= '<tr>';
          $html .= '<td>';
            $html .= '<span>'.$section['whair_pricingpage_pricing_table_item'].'</span>';
          $html .= '</td>';
          $html .= '<td>';
            $html .= '<span>'.$section['whair_pricingpage_pricing_table_price'].'</span>';
          $html .= '</td>';
        $html .= '</tr>';
      $html .= '</table>';
   }

   return $html;
}

function draw_pricing_table_two($draw_pricing_table_two) {
    $html = '';

    foreach ($draw_pricing_table_two as $section) {
      $html .= '<table>';
        $html .= '<tr>';
          $html .= '<td>';
            $html .= '<span>'.$section['whair_pricingpage_pricing_table_item'].'</span>';
          $html .= '</td>';
          $html .= '<td>';
            $html .= '<span>'.$section['whair_pricingpage_pricing_table_price'].'</span>';
          $html .= '</td>';
        $html .= '</tr>';
      $html .= '</table>';
   }

   return $html;
}

function draw_pricing_table_three($draw_pricing_table_three) {
    $html = '';

    foreach ($draw_pricing_table_three as $section) {
      $html .= '<table>';
        $html .= '<tr>';
          $html .= '<td>';
            $html .= '<span>'.$section['whair_pricingpage_pricing_table_item'].'</span>';
          $html .= '</td>';
          $html .= '<td>';
            $html .= '<span>'.$section['whair_pricingpage_pricing_table_price'].'</span>';
          $html .= '</td>';
        $html .= '</tr>';
      $html .= '</table>';
   }

   return $html;
}

function draw_pricing_table_four($draw_pricing_table_four) {
    $html = '';

    foreach ($draw_pricing_table_four as $section) {
      $html .= '<table>';
        $html .= '<tr>';
          $html .= '<td>';
            $html .= '<span>'.$section['whair_pricingpage_pricing_table_item'].'</span>';
          $html .= '</td>';
          $html .= '<td>';
            $html .= '<span>'.$section['whair_pricingpage_pricing_table_price'].'</span>';
          $html .= '</td>';
        $html .= '</tr>';
      $html .= '</table>';
   }

   return $html;
}

function draw_pricing_table_five($draw_pricing_table_five) {
    $html = '';

    foreach ($draw_pricing_table_five as $section) {
      $html .= '<table>';
        $html .= '<tr>';
          $html .= '<td>';
            $html .= '<span>'.$section['whair_pricingpage_pricing_table_item'].'</span>';
          $html .= '</td>';
          $html .= '<td>';
            $html .= '<span>'.$section['whair_pricingpage_pricing_table_price'].'</span>';
          $html .= '</td>';
        $html .= '</tr>';
      $html .= '</table>';
   }

   return $html;
}

function draw_pricing_tables($draw_pricing_tables) {
    $html = '';

    foreach ($draw_pricing_tables as $section) {
    $html .= '<div class="pricing-title">';
      $html .= '<h2>'.$section['whair_pricingpage_pricing_table_title'].'</h2>';
    $html .= '</div>';
    $html .= '<hr align="left">';
    $html .= '<div class="pricing-content">';
      $html .= '<p>'.$section['whair_pricingpage_pricing_table_content'].'</p>';
    $html .= '</div>';
   }

   return $html;
}

// Register Custom Post Type
function staff() {

	$labels = array(
		'name'                  => _x( 'Staff Profiles', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Staff Profile', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Staff Profiles', 'text_domain' ),
		'name_admin_bar'        => __( 'Staff Profile', 'text_domain' ),
		'archives'              => __( 'Staff Profiles Archives', 'text_domain' ),
		'attributes'            => __( 'Staff Profile Attributes', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
		'all_items'             => __( 'All Staff Profiles', 'text_domain' ),
		'add_new_item'          => __( 'Add New Staff Profile', 'text_domain' ),
		'add_new'               => __( 'Add New Staff Profile', 'text_domain' ),
		'new_item'              => __( 'New Staff Profile', 'text_domain' ),
		'edit_item'             => __( 'Edit Staff Profile', 'text_domain' ),
		'update_item'           => __( 'Update Staff Profile', 'text_domain' ),
		'view_item'             => __( 'View Staff Profile', 'text_domain' ),
		'view_items'            => __( 'View Staff Profiles', 'text_domain' ),
		'search_items'          => __( 'Search Staff Profile', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into Staff Profile', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Staff Profile', 'text_domain' ),
		'items_list'            => __( 'Staff Profiles list', 'text_domain' ),
		'items_list_navigation' => __( 'Staff Profiles list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter Staff Profiles list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Staff Profile', 'text_domain' ),
		'description'           => __( 'Staff Profiles', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'revisions', ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-groups',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
    'rewrite'               => array('slug' => 'meet-the-team'),
    'has_archive'           => false,
	);
	register_post_type( 'meet-the-team', $args );

}
add_action( 'init', 'staff', 0 );
