<?php
/**
 * Template Name: Team Page Template
 */
 $page_id = get_the_ID();

 $featured_image = wp_get_attachment_image_src(get_post_thumbnail_id($page_id), 'single-post-thumbnail');

 $metabox_id_array = array(
   'whair_aboutpage_secondary_section_head_content',
   'whair_aboutpage_secondary_section_content',
   'whair_aboutpage_floating_image_right',
   'whair_aboutpage_floating_image_left',
   'whair_aboutpage_final_content_section',
 );

 $metabox_content_array = get_metabox_content($page_id, $metabox_id_array);
 ?>

 <?php while (have_posts()) : the_post(); ?>
   <div class="hero-wrapper" style="background-image:url('<?php echo $featured_image[0] ?>');">
     <div class="hero-wrapper-verticle">
       <div style="display: table-cell; vertical-align: middle;">
         <div class="container">
           <div class="row">
             <div class="col-lg-8 mx-auto">
     		      <div class="hero-content"><?php echo the_content() ?></div>
             </div>
           </div>
         </div>
         <div class="down-arrow fade-3s">
           <div id="scroll-down">
             <span class="arrow-down">
             <!-- css generated icon -->
             </span>
           </div>
         </div>
       </div>
     </div>
   </div>

<div class="team-profiles-section">
  <div class="team-profiles-container">
    <div class="row">

      <?php
        $args = array(
          'post_type' => 'meet-the-team',
          'orderby' => 'DESC',
          'posts_per_page' => -1
          );
          // the query
        $the_query = new WP_Query( $args );
      ?>

      <?php if ( $the_query->have_posts() ) : ?>

      <!-- pagination here -->

      <!-- the loop -->
      <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

      <div class="team-col col-lg-4 box">
        <div class="team-img img-fluid mx-auto d-block"><?php the_post_thumbnail(); ?></div>
        <h2 class="team-title"><?php the_title(); ?></h2>
        <div class="team-title"><?php the_content(); ?></div>
        <hr align="left">
      </div>

      <?php endwhile; ?>
      <!-- end of the loop -->

      <!-- pagination here -->

      <?php wp_reset_postdata(); ?>

      <?php else : ?>
      <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
      <?php endif; ?>

    </div>
  </div>
</div>
<?php endwhile; ?>
