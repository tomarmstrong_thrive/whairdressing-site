<?php
/**
 * Template Name: Services Page Template
 */
 $page_id = get_the_ID();

 $featured_image = wp_get_attachment_image_src(get_post_thumbnail_id($page_id), 'single-post-thumbnail');

 $metabox_id_array = array(
   'whair_servicespage_secondary_section_head_content',
   'whair_servicespage_secondary_section_content',
   'whair_servicespage_secondary_section_image',
 );

 $metabox_content_array = get_metabox_content($page_id, $metabox_id_array);
 ?>

 <?php while (have_posts()) : the_post(); ?>
   <div class="hero-wrapper" style="background-image:url('<?php echo $featured_image[0] ?>');">
     <div class="hero-wrapper-verticle">
       <div style="display: table-cell; vertical-align: middle;">
         <div class="container">
           <div class="row">
             <div class="col-lg-8 mx-auto">
     		      <div class="hero-content"><?php echo the_content() ?></div>
             </div>
           </div>
         </div>
         <div class="down-arrow fade-3s">
           <div id="scroll-down">
             <span class="arrow-down">
             <!-- css generated icon -->
             </span>
           </div>
         </div>
       </div>
     </div>
   </div>

   <div class="secondary-image-split">
     <div class="row">
       <div class="col-xl-6 col-lg-12">
         <div class="hero-wrapper-verticle">
           <div style="display: table-cell; vertical-align: middle;">
             <?php echo wpautop($metabox_content_array['whair_servicespage_secondary_section_head_content']); ?>
             <hr align="left">
             <?php echo wpautop($metabox_content_array['whair_servicespage_secondary_section_content']); ?>
            </div>
         </div>
       </div>
       <div class="col-lg-6 secondary-image d-none d-xl-block" style="background-image:url('<?php echo ($metabox_content_array['whair_servicespage_secondary_section_image']); ?>');">
       </div>
     </div>
   </div>
<?php endwhile; ?>
