<?php
/**
 * Template Name: Contact Page Template
 */
 $page_id = get_the_ID();

 $prefix = 'whair_contactpage_';

 $featured_image = wp_get_attachment_image_src(get_post_thumbnail_id($page_id), 'single-post-thumbnail');

 $metabox_id_array = array(
  'whair_contactpage_contact_content',
  'whair_contactpage_contact_floating_img',


  'whair_contactpage_tripple_cta_content',
  'whair_contactpage_cta_image_left',
  'whair_contactpage_cta_link_left',
  'whair_contactpage_cta_image_center',
  'whair_contactpage_cta_link_center',
  'whair_contactpage_cta_image_right',
  'whair_contactpage_cta_link_right',
 );

 $metabox_content_array = get_metabox_content($page_id, $metabox_id_array);


 $pricing_tables= get_post_meta($page_id, $prefix.'pricing_tables', true);
?>

<?php while (have_posts()) : the_post(); ?>
  <div class="hero-wrapper" style="background-image:url('<?php echo $featured_image[0] ?>');">
    <div class="hero-wrapper-verticle">
      <div style="display: table-cell; vertical-align: middle;">
        <div class="container">
          <div class="row">
            <div class="col-lg-8 mx-auto">
             <div class="hero-content"><?php echo the_content() ?></div>
            </div>
          </div>
        </div>
        <div class="down-arrow fade-3s">
          <div id="scroll-down">
            <span class="arrow-down">
            <!-- css generated icon -->
            </span>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="contact-main-section">
    <div class="row">
      <div class="col-lg-5">
        <div class="contact-information">
          <?php dynamic_sidebar('sidebar-footer'); ?>
          <div class="contact-floating-img mouse-move-one d-sm-none d-md-block">
            <img src="<?php echo ($metabox_content_array['whair_contactpage_contact_floating_img']); ?>" class="img-fluid float-left fade-1s"/>
          </div>
        </div>
      </div>
      <div class="col-lg-7">
        <div class="contact-form-wrapper">
          <?php echo wpautop($metabox_content_array['whair_contactpage_contact_content']); ?>
          <hr align="left">
          <?php echo do_shortcode('[contact-form-7 id="6" title="Contact form 1"]') ?>
        </div>
      </div>
    </div>
  </div>

  <div class="tripple-cta-section">
    <div class="container">
      <div class="hero-wrapper-verticle">
        <div style="display: table-cell; vertical-align: middle;">
          <div class="row">
            <div class="col-lg-12">
              <?php echo wpautop($metabox_content_array['whair_contactpage_tripple_cta_content']); ?>
              <hr align="left">
            </div>
            <div class="col-lg-4 col-md-12 col-sm-4 three-cta-col">
              <a href="<?php echo ($metabox_content_array['whair_contactpage_cta_link_left']); ?>"><div class="img-hover"><img src="<?php echo ($metabox_content_array['whair_contactpage_cta_image_left']); ?>" class="img-fluid mx-auto d-block"/><div class="overlay"></div></div></a>
            </div>
            <div class="col-lg-4 col-md-12 col-sm-4 three-cta-col">
              <a href="<?php echo ($metabox_content_array['whair_contactpage_cta_link_center']); ?>"><div class="img-hover"><img src="<?php echo ($metabox_content_array['whair_contactpage_cta_image_center']); ?>" class="img-fluid mx-auto d-block"/><div class="overlay"></div></div></a>
            </div>
            <div class="col-lg-4 col-md-12 col-sm-4 three-cta-col">
              <a href="<?php echo ($metabox_content_array['whair_contactpage_cta_link_right']); ?>"><div class="img-hover"><img src="<?php echo ($metabox_content_array['whair_contactpage_cta_image_right']); ?>" class="img-fluid mx-auto d-block"/><div class="overlay"></div></div></a>
            </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php endwhile; ?>
