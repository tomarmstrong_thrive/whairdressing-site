<?php
/**
 * Template Name: Pricing Page Template
 */
  $page_id = get_the_ID();

  $prefix = 'whair_pricingpage_';

  $featured_image = wp_get_attachment_image_src(get_post_thumbnail_id($page_id), 'single-post-thumbnail');

  $metabox_id_array = array(
   'whair_pricingpage_pricing_table_one_title',
   'whair_pricingpage_pricing_table_two_title',
   'whair_pricingpage_pricing_table_three_title',
   'whair_pricingpage_pricing_table_three_ac',
   'whair_pricingpage_pricing_table_four_title',
   'whair_pricingpage_pricing_table_five_title',

   'whair_pricingpage_floating_img_top',
   'whair_pricingpage_floating_img_bottom',

   'whair_pricingpage_team_cta_title',
   'whair_pricingpage_team_cta_content',
   'whair_pricingpage_team_cta_button_text',
   'whair_pricingpage_team_cta_button_link',
   'whair_pricingpage_team_cta_image',
  );

  $metabox_content_array = get_metabox_content($page_id, $metabox_id_array);


  $pricing_table_one= get_post_meta($page_id, $prefix.'pricing_table_one', true);
  $pricing_table_two= get_post_meta($page_id, $prefix.'pricing_table_two', true);
  $pricing_table_three= get_post_meta($page_id, $prefix.'pricing_table_three', true);
  $pricing_table_four= get_post_meta($page_id, $prefix.'pricing_table_four', true);
  $pricing_table_five= get_post_meta($page_id, $prefix.'pricing_table_five', true);
 ?>

 <?php while (have_posts()) : the_post(); ?>
   <div class="hero-wrapper" style="background-image:url('<?php echo $featured_image[0] ?>');">
     <div class="hero-wrapper-verticle">
       <div style="display: table-cell; vertical-align: middle;">
         <div class="container">
           <div class="row">
             <div class="col-lg-8 mx-auto">
     		      <div class="hero-content"><?php echo the_content() ?></div>
             </div>
           </div>
         </div>
         <div class="down-arrow fade-3s">
           <div id="scroll-down">
             <span class="arrow-down">
             <!-- css generated icon -->
             </span>
           </div>
         </div>
       </div>
     </div>
   </div>

  <div class="pricing-secondary-section">
    <div class="row">
      <div class="col-xl-5 col-lg-12 pricing-secondary-section-inner">
        <div class="pricing-table">
          <div class="pricing-title">
            <h2><?php echo ($metabox_content_array['whair_pricingpage_pricing_table_one_title']); ?></h2>
          </div>
          <hr align="left">
          <?php echo draw_pricing_table_one($pricing_table_one); ?>
        </div>
        <div class="pricing-table">
          <div class="pricing-title">
            <h2><?php echo ($metabox_content_array['whair_pricingpage_pricing_table_two_title']); ?></h2>
          </div>
          <hr align="left">
          <?php echo draw_pricing_table_two($pricing_table_two); ?>
        </div>
        <div class="pricing-table">
          <div class="pricing-title">
            <h2><?php echo ($metabox_content_array['whair_pricingpage_pricing_table_three_title']); ?></h2>
          </div>
          <hr align="left">
          <?php echo draw_pricing_table_three($pricing_table_three); ?>
          <div class="pricing-ac">
            <?php echo ($metabox_content_array['whair_pricingpage_pricing_table_three_ac']); ?>
          </div>
        </div>
        <div class="pricing-table">
          <div class="pricing-title">
            <h2><?php echo ($metabox_content_array['whair_pricingpage_pricing_table_four_title']); ?></h2>
          </div>
          <hr align="left">
          <?php echo draw_pricing_table_four($pricing_table_four); ?>
        </div>
        <div class="pricing-table">
          <div class="pricing-title">
            <h2><?php echo ($metabox_content_array['whair_pricingpage_pricing_table_five_title']); ?></h2>
          </div>
          <hr align="left">
          <?php echo draw_pricing_table_five($pricing_table_five); ?>
        </div>
      </div>
      <div class="col-xl-1 d-block d-none d-xl-block">
      </div>
      <div class="col-xl-6 image-col d-none d-xl-block">
        <img src="<?php echo ($metabox_content_array['whair_pricingpage_floating_img_top']); ?>" class="img-fluid float-left fade-1s pricing-img-top"/>
        <img src="<?php echo ($metabox_content_array['whair_pricingpage_floating_img_bottom']); ?>" class="img-fluid float-right fade-1p5s pricing-img-bottom"/>

        <div class="prices-page-cta">
          <div class="floating-img-two mouse-move-one">
            <img src="<?php echo ($metabox_content_array['whair_pricingpage_team_cta_image']); ?>" class="img-fluid float-right footer-two"/>
          </div>
          <div class="row">
            <div class="col-lg-9">
              <h2><?php echo ($metabox_content_array['whair_pricingpage_team_cta_title']); ?></h2>
              <hr>
              <?php echo wpautop($metabox_content_array['whair_pricingpage_team_cta_content']); ?>
              <a href="<?php echo ($metabox_content_array['whair_pricingpage_team_cta_button_link']); ?>"><button class="btn btn-marg-top"><p><?php echo ($metabox_content_array['whair_pricingpage_team_cta_button_text']); ?></p></button></a>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-12 image-col d-xl-none">
        <div class="prices-page-cta">
          <div class="floating-img-two mouse-move-one  d-md-none d-sm-none d-xs-none">
            <img src="<?php echo ($metabox_content_array['whair_pricingpage_team_cta_image']); ?>" class="img-fluid float-right footer-two"/>
          </div>
          <div class="row">
            <div class="col-lg-9">
              <h2><?php echo ($metabox_content_array['whair_pricingpage_team_cta_title']); ?></h2>
              <hr align="left">
              <?php echo wpautop($metabox_content_array['whair_pricingpage_team_cta_content']); ?>
              <a href="<?php echo ($metabox_content_array['whair_pricingpage_team_cta_button_link']); ?>"><button class="btn btn-marg-top"><p><?php echo ($metabox_content_array['whair_pricingpage_team_cta_button_text']); ?></p></button></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php endwhile; ?>
